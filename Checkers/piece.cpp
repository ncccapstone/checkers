#include <iomanip>;
#include <iostream>;
#include <Windows.h>;

#include "piece.h";

using namespace std;

bool isValid(Piece selected, COORD newLocation)
{
	if (selected.getX
};

Piece::Piece(COORD location, Color checker)
{

	isKing() = 0;
	color = checker;	
}

//get numerical value of piece
int Piece::getValue(void)
{
	int value = 0;
	
	if (getColor() == Red)
	{
		value += 1;
	}
	else
	{
		value += 2;
	}
	
	if (isKing)
	{
		value += 2;
	}
	
	return value;
}

void Piece::setLocation(int x, int y)
{
	location.X = x;
	location.Y = y;
}

COORD Piece::getLocation(void)
{
	return location;
}

void Piece::movePiece(Piece selected, COORD newLocation)
{
	if (isValid(selected, newLocation))
	{
	
		board(selected.location) = 0;
		board[newLocation.x][newLocation.y] = selected.getValue;
	}
	else
	{
		cout << "This move is not valid, please choose another move";
	}
}

Color Piece::getColor()
{
	return color;
}
	

