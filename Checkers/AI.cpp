﻿/*
* Module Name: AI.cpp
*
* Description: AI logic for computer player (MiniMax, Negamax)
*
* Author: Team C++
*
* Date: 02NOV2015
*
* Modification History:
*   02NOV2015 - Initial file creation - DA
*	09NOV2015 - Further work on negamax algorithm - DA
*	16NOV2015 - Additional work on return data and AI functions - DA
*	30NOV2015 - Updated functions to use MiniMax algorithm
*	06DEC2015 - AI improvements including setting depth to 6, checking the possible moves for human and switching player's turn - DA
*/

#include "GameBoard.h"
#include "AI.h"

#include <fstream>
#include <string>
#include <list>

#define DEBUG_GAME

// Default constructor
AI::AI()
{

}

// Evaluates the board calculating a score based on the number of regular and king pieces
int AI::Evaluate(GameBoard state)
{
	auto turn = state.getPlayersTurn();

	int RedScore = 0 ,
		BlackScore = 0;

	int piece;

	for ( int y = 0; y < 8; ++y )
		for ( int x = 0; x < 8; ++x )
		{
			piece = state.getBoardState(x , y);

			if ( piece == Red )
			{
				RedScore += 2;
				if ( y == 0 || y == 7 ) RedScore += 1;
			}
			else if ( piece == RedKing )
			{
				RedScore += 4;
				if ( y == 0 || y == 7 ) RedScore += 1;
			}

			else if ( piece == Black )
			{
				BlackScore += 2;
				if ( y == 0 || y == 7 ) BlackScore += 1;
			}
			else if ( piece == BlackKing )
			{
				BlackScore += 4;
				if ( y == 0 || y == 7 ) BlackScore += 1;
			}

		}

	if ( state.getPlayersTurn() == Computer )
		return RedScore - BlackScore;

	return BlackScore - RedScore;
}

// Get list of valid double jumps
std::list<GameBoard> AI::GetDoubleJumps(GameBoard state , COORD jumpingPiece)
{
	std::list<GameBoard> validJumps;
	COORD newPos;

	GameBoard originalState = state;

	for ( int y = 0; y < 8; y++ )
	{
		for ( int x = 0; x < 8; x++ )
		{
			newPos.X = x;
			newPos.Y = y;
			if ( state.validMove(jumpingPiece , newPos) )
			{
				GameBoard jumpBoard = originalState;
				jumpBoard.movePiece(jumpingPiece , newPos);
				validJumps.push_back(jumpBoard);
				state = originalState;
			}
		}
	}

	return validJumps;
}

// Build a List of all possible moves from a given board state
std::list<GameBoard> AI::possibleMoves(GameBoard &state)
{
	COORD oldPos;
	COORD newPos;

	std::list<GameBoard> returnData;

	for ( int y = 0; y < 8; y++ )
	{
		for ( int x = 0; x < 8; x++ )
		{
			auto currentPiece = state.getBoardState(x , y);
			if ( state.getPlayersTurn() == Computer && (currentPiece == Red || currentPiece == RedKing) )
			{
				auto tempBoard = state;

				for ( auto y2 = 0; y2 < 8; y2++ )
				{
					for ( auto x2 = 0; x2 < 8; x2++ )
					{
						bool isJump = false;
						oldPos.X = x;
						oldPos.Y = y;

						newPos.X = x2;
						newPos.Y = y2;

						if ( tempBoard.validMove(oldPos , newPos) )
						{
							isJump = tempBoard.jump(oldPos , newPos);
							tempBoard.movePiece(oldPos , newPos);
							returnData.push_back(tempBoard);

							// Get any double jumps and add them to the list of valid moves
							if ( isJump )
							{
								auto doubleJumps = GetDoubleJumps(tempBoard , newPos);
								if ( doubleJumps.size() > 0 )
								{
									returnData.insert(returnData.end() , doubleJumps.begin() , doubleJumps.end());
								}
							}
							tempBoard.setPlayersTurn(Human);
							tempBoard = state;
						}
					}
				}
			}

			else if ( state.getPlayersTurn() == Human && (currentPiece == Black || currentPiece == BlackKing) )
			{
				GameBoard tempBoard = state;

				for ( auto y2 = 7; y2 >= 0; y2-- )
				{
					for ( auto x2 = 7; x2 >= 0; x2-- )
					{
						bool isJump = false;

						oldPos.X = x;
						oldPos.Y = y;

						newPos.X = x2;
						newPos.Y = y2;

						if ( tempBoard.validMove(oldPos , newPos) )
						{
							isJump = tempBoard.jump(oldPos , newPos);
							tempBoard.movePiece(oldPos , newPos);
							returnData.push_back(tempBoard);

							// Get any double jumps and add them to the list of valid moves
							if ( isJump )
							{
								auto doubleJumps = GetDoubleJumps(tempBoard , newPos);
								if ( doubleJumps.size() > 0 )
								{
									returnData.insert(returnData.end() , doubleJumps.begin() , doubleJumps.end());
								}
							}
							tempBoard.setPlayersTurn(Computer);
							tempBoard = state;
						}
					}
				}
			}

		}
	}

	return returnData;
}

// MiniMax algorithm 
int AI::MiniMax(const GameBoard &state , int depth , int alpha , int beta , bool maximizer)
{
	GameBoard gb = state;
	if ( depth == 0 || gb.isGameOver() )
	{
		//state.setPlayersTurn(Computer);
		return AI::Evaluate(state);
	}
	std::list<GameBoard> possibleMoves;
	possibleMoves = AI::possibleMoves(gb);

	if ( maximizer )
	{
		for ( const GameBoard &move : possibleMoves )
		{
			alpha = max(alpha , MiniMax(move , depth - 1 , alpha , beta , false));
			if ( beta <= alpha )
				return beta;
		}
		return alpha;
	}
	else
	{
		for ( const GameBoard &move : possibleMoves )
		{
			beta = min(beta , MiniMax(move , depth - 1 , alpha , beta , true));
			if ( beta <= alpha )
				return alpha;
		}
		return beta;
	}
}

// BestMove - Gets the best possible move for a given board state
GameBoard AI::BestMove(GameBoard pState)
{
	GameBoard bestMove;

	int alpha = -1000;
	int beta = 1000;

	std::list<GameBoard> possibleMoves;
	possibleMoves = AI::possibleMoves(pState);

#ifdef DEBUG_GAME
	// Outputs the possible moves and their score to a file
	std::ofstream saveFile("debug.dat");
	if ( saveFile.is_open() )
	{
		for ( GameBoard move : possibleMoves )
		{
			int Evaluation = AI::Evaluate(move);
			for ( int y = 0; y < 8; ++y )
			{
				for ( int x = 0; x < 8; ++x )
				{
					int boardState = move.getBoardState(x , y);
					if ( boardState != 0 )
						saveFile << boardState;
					else
						saveFile << ".";
				}
				saveFile << std::endl;
			}
			saveFile << "Score: " << std::to_string(Evaluation);
			saveFile << std::endl << std::endl << std::endl;
		}

		saveFile.close();
	}
#endif

	for ( GameBoard move : possibleMoves )
	{
		int currentBest = AI::MiniMax(move , 0 , alpha , beta , true);

		if ( currentBest > alpha )
		{
			alpha = currentBest;
			bestMove = move;
		}
	}

	return bestMove;
}

// Default Destructor
AI::~AI()
{

}

