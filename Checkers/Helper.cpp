/*
* Module Name: Helper.cpp
*
* Description: Implementation file for Helper class
*
* Author: Team C++
*
* Date: 26OCT2015
*
* Modification History:
*   26OCT2015 - Initial file creation - DA
*	16NOV2015 - Added displayMessage function - DA
*	23NOV2015 - Improved message display - GR
*/

#include <iostream>
#include <string>
#include <Windows.h>

#include "Helper.h"

// Sets the font size for the console
BOOL Helper::SetConsoleFontSize(COORD dwFontSize) 
{
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_FONT_INFOEX info{ sizeof(CONSOLE_FONT_INFOEX) };
	if ( !GetCurrentConsoleFontEx(output , false , &info) )
		return false;
	info.dwFontSize = dwFontSize;
	return SetCurrentConsoleFontEx(output , false , &info);
}

// Hide console cursor
void Helper::ShowConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out , &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out , &cursorInfo);
}

// Clears the input area (where filenames are entered)
void Helper::clearTextInput()
{
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	COORD pos;
	pos.X = 20;
	pos.Y = 1;

	SetConsoleCursorPosition(hOut , pos);
	std::cout << "                                    ";

}

// Displays a message below the board
void Helper::displayMessage(std::string message)
{
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	COORD pos;
	pos.X = 0;
	pos.Y = 17;

	SetConsoleCursorPosition(hOut , pos);
	std::cout << "                                                                                            "
		<< "                                                                                                                ";
	SetConsoleCursorPosition(hOut , pos);
	std::cout << message;
}

// Prints the board not including the pieces
void Helper::PrintBoard()
{
	COORD fontSize;
	fontSize.X = 16;
	fontSize.Y = 12;
	SetConsoleFontSize(fontSize);

	std::cout << TL << HL << TB << HL << TB << HL << TB << HL << TB << HL << TB << HL << TB << HL << TB << HL << TR << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << LB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << IB << HL << RB << "\n";
	std::cout << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << " " << VL << "\n";
	std::cout << BL << HL << BB << HL << BB << HL << BB << HL << BB << HL << BB << HL << BB << HL << BB << HL << BR << std::endl;
}

// Translate from array location to screen location
COORD Helper::arrayToScreen(COORD arrayLocation)
{
	COORD returnValue;

	returnValue.Y = (arrayLocation.Y * 2) + 1;
	returnValue.X = (arrayLocation.X * 2) + 1;

	return returnValue;
}

// Convert screen coordinates to array indexs
COORD Helper::screenToArray(COORD screenLocation)
{
	COORD returnValue;
		
	returnValue.Y = (screenLocation.Y / 2);
	returnValue.X = (screenLocation.X / 2);

	return returnValue;
}