/*
* Module Name: GameBoard.h
*
* Description: Header file for GameBoard class
*
* Author: Team C++
*
* Date: 19OCT2015
*
* Modification History:
*   19OC2015 - Initial file creation
*	25OCT2015 - Added getBoardState function - GR
*	26OCT2015 - Added load/save game functions and Turn enum.  Moved some functions to Helper class - DA
*	02NOV2015 - Added setBoardState function - GR
*	09NOV2015 - Added #pragma once - DA
*	16NOV2015 - Added getter and setter for Turn - DA
*	16NOV2015 - Moved all move and validation functions to here from main application code, doubleJump improvements - GR
*	17NOV2015 - Added revertJump function - GR
*	22NOV2015 - Improved validMove, validJump and doubleJump. - GR
*	06DEC2015 - Removed messaging from validMove function to improve AI - DA
*/

#pragma once

#include <iostream>

#include "Helper.h"

class GameBoard
{
public:
	GameBoard();

	void printBoard();

	void loadGame(std::string);
	void saveGame(std::string);

	Turn getPlayersTurn();
	void setPlayersTurn(Turn);

	int getBoardState(int , int);
	void setBoardState(int , int , int);
	bool isGameOver(void);

	bool emptySpace(int , int);
	bool validMove(COORD , COORD);
	bool validJump(int , int , int , int);
	void removePiece(int , int , int , int);
	void movePiece(COORD , COORD);
	bool doubleJump(COORD , COORD);
	bool jump(COORD , COORD);
	void revertJump(COORD , COORD);


	~GameBoard();

private:
	bool gameOver;

	Turn turn;

	int boardState[8][8];
};
