/*
* Module Name: Checkers.cpp
*
* Description: Main program to play checkers
*
* Author: Team C++
*
* Date: 03SEP2015
*
* Modification History:
*   03SEP2015 - Initial file creation
*	19OCT2015 - Add PrintBoard() and SetConsoleFontSize() - DA
*	26OCT2015 - Add validMove() - GR, Move some functions and definitions to Helper class - DA
*	01NOV2015 - Added #defines for moves, bool functions to assist with validMove() - GR
*	02NOV2015 - Added emptySpace and validRedJump\validBlackJump functions - GR
*	03NOV2015 - Changed variable names and replaced Red/Black jump functions with validJump - GR, Added String - DA
*	08NOV2015 - Updated validJump and validMove - GR
*	09NOV2015 - Completed validMove and validJump, added doubleJump - GR, Removed unnecessary code and improved mouse event handling - DA
*	10NOV2015 - Added removePiece function - GR
*	12NOV2015 - Fixed bugs in validMove and validJump. - GR
*	16NOV2015 - Added code to turn piece into king.  Changed pieceSelected to improve functionality. Moved all functions to gameBoard class - GR
*	16NOV2015 - Added computer's turn and improved message display - DA
*	17NOV2015 - Work on moving pieces - GR
*	22NOV2015 - Continued work on moving pieces. - GR
*	23NOV2015 - Piece move improvements, additional messaging - GR
*	28NOV2015 - Adjusted piece movement to ensure next move is a jump when double jumping - GR
*	29NOV2015 - Edited piece selection to ensure that the piece selected is the correct color - GR
*	30NOV2015 - Added stack to enable reverting board back to original state if double-jumps are cancelled - GR
*	06DEC2015 - Added AI to main application - DA
*/

#include <windows.h>
#include <iostream>
#include <string>
#include <stack>

#include "GameBoard.h"
#include "Helper.h"
#include "AI.h"

using namespace std;

bool PieceSelected;
stack<GameBoard> boards;


int main(int argc , char *argv[])
{
	GameBoard game;
	string message;
	int jump = 0;  //used to denote if this move is part of a double-jump process

	Helper::ShowConsoleCursor(false);
	Helper::PrintBoard();

	HANDLE hIn;
	HANDLE hOut;

	auto Continue = true;

	INPUT_RECORD InRec;
	DWORD NumRead;

	hIn = GetStdHandle(STD_INPUT_HANDLE);
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	message = "Welcome to King Me!  Please double-click to choose a piece to move or \n choose \"l\" to load a game. Press \"s\" at any time to save the game.\n";
	Helper::displayMessage(message);

	game.printBoard();

	while ( !game.isGameOver() )
	{
		if ( game.getPlayersTurn() == Human )
		{
			PieceSelected = false;
			COORD selectedPiece = {};
			COORD desiredPosition;

			while ( Continue )
			{
				ReadConsoleInput(hIn , &InRec , 1 , &NumRead);

				switch ( InRec.EventType )
				{
				case KEY_EVENT:
					if ( InRec.Event.KeyEvent.uChar.AsciiChar == ESC )
					{
						Continue = false;
					}

					else if ( InRec.Event.KeyEvent.uChar.AsciiChar == 's' ||
						InRec.Event.KeyEvent.uChar.AsciiChar == 'S' )
					{
						string fileName;

						COORD pos;
						pos.X = 20;
						pos.Y = 1;

						SetConsoleCursorPosition(hOut , pos);
						cout << "FileName: ";
						cin >> fileName;
						game.saveGame(fileName);
						Helper::clearTextInput();
					}
					else if ( InRec.Event.KeyEvent.uChar.AsciiChar == 'l' ||
						InRec.Event.KeyEvent.uChar.AsciiChar == 'L' )
					{
						string fileName;

						COORD pos;
						pos.X = 20;
						pos.Y = 1;

						SetConsoleCursorPosition(hOut , pos);
						cout << "FileName: ";
						cin >> fileName;
						game.loadGame(fileName);
						game.printBoard();
						Helper::clearTextInput();
					}

					break;

				case MOUSE_EVENT:
					if ( InRec.Event.MouseEvent.dwEventFlags & DOUBLE_CLICK )
					{
						COORD pos;
						pos.X = InRec.Event.MouseEvent.dwMousePosition.X;
						pos.Y = InRec.Event.MouseEvent.dwMousePosition.Y;

						if ( (pos.X < 16 && (pos.X % 2 != 0)) &&
							(pos.Y < 16 && (pos.Y % 2 != 0)) )
						{
							SetConsoleCursorPosition(hOut , pos);
							if ( PieceSelected )
							{
								message.erase(0 , message.length());
								Helper::displayMessage(message);

								desiredPosition = Helper::screenToArray(pos);

								//allow player to de-select original piece
								if ( (selectedPiece.X == desiredPosition.X) &&
									(selectedPiece.Y == desiredPosition.Y) && (jump == 0) )
								{

									PieceSelected = false;
									Continue = true;
								}
								//as long as it's a jump and valid move, check if there's a double jump available
								else if ( game.jump(selectedPiece , desiredPosition) && game.validMove(selectedPiece , desiredPosition) )
								{


									//if piece is getting kinged, don't check for double jump
									if ( ((game.getBoardState(selectedPiece.X , selectedPiece.Y)) == Red) && (desiredPosition.Y == 7) ||
										(game.getBoardState(selectedPiece.X , selectedPiece.Y) == Black) && (desiredPosition.Y == 0) )
									{
										game.movePiece(selectedPiece , desiredPosition);
										PieceSelected = false;
										game.printBoard();
										game.setPlayersTurn(Computer);
										Continue = false;
										jump = 0;
									}
									else if ( game.doubleJump(selectedPiece , desiredPosition) )
									{

										game.printBoard();
										char choice;

										message.erase(0 , message.length());
										message = "There is a jump available after this move.  Please press \"c\" to continue and\nchoose the next jump or \"p\" to select another piece to move\n";
										Helper::displayMessage(message);
										cin >> choice;

										//p chooses new piece to move, c continues to choose next jump
										if ( (choice == 'p') || (choice == 'P') )
										{
											for ( int i = 1; i < jump; i++ )
											{
												boards.pop();
											}
											game = boards.top();

											PieceSelected = false;
											game.printBoard();
											message.erase(0 , message.length());
											Helper::displayMessage(message);
											int size = boards.size();
											for ( int b = 0; b < size; b++ )
											{
												boards.pop();
											}
											jump = 0;
											break;
										}

										if ( (choice == 'c') || (choice == 'C') )
										{
											//keep a stack of gameboards in case the move is cancelled
											boards.push(game);
											message.erase(0 , message.length());
											Helper::displayMessage(message);
											game.movePiece(selectedPiece , desiredPosition);
											selectedPiece = desiredPosition;
											PieceSelected = true;
											game.printBoard();
											message = "Please choose the next spot to jump to";
											Helper::displayMessage(message);
											jump += 2;
											boards.push(game);
											break;
										}

										Helper::displayMessage("Please choose \"c\" to choose the next jump or \"p\" to select another piece");
									}
									else if ( game.validMove(selectedPiece , desiredPosition) )
									{
										game.movePiece(selectedPiece , desiredPosition);
										PieceSelected = false;
										game.printBoard();
										game.setPlayersTurn(Computer);
										Continue = false;
										jump = 0;
									}
									else
									{
										Helper::displayMessage("Not a valid move.  Please choose another");
										PieceSelected = false;
										Continue = true;
									}
								}
								//if it's not a jump or part of a double-jump and it's valid, move the piece
								else if ( (game.validMove(selectedPiece , desiredPosition)) && (jump == 0) )
								{
									game.movePiece(selectedPiece , desiredPosition);
									PieceSelected = false;
									game.printBoard();
									game.setPlayersTurn(Computer);
									Continue = false;
								}
								//if this is part of a double-jump, force the next move to be a jump
								else if ( jump > 0 )
								{
									Helper::displayMessage("Next move must be a jump.  Please choose place to jump to.");
									PieceSelected = true;
									Continue = true;
								}
								else
								{
									Helper::displayMessage("Not a valid move.  Please choose another");
									PieceSelected = false;
									Continue = true;
								}
							}
							else
							{
								selectedPiece = Helper::screenToArray(pos);

								//Make sure that the player is choosing its own piece
								if ( game.getBoardState(selectedPiece.X , selectedPiece.Y) % 2 != 0 )
								{
									message = "Please choose a white piece";
									Helper::displayMessage(message);
									PieceSelected = false;
								}
								else if ( game.getBoardState(selectedPiece.X , selectedPiece.Y) == Empty )
								{
									message = "That space is empty.  Please choose a piece to move.";
									Helper::displayMessage(message);
									PieceSelected = false;
								}
								else
								{
									PieceSelected = true;
									message = "Piece selected...please choose the destination";
									Helper::displayMessage(message);
								}

							}
						}
					}
					break;
				}
			}
		}
		else if ( game.getPlayersTurn() == Computer )
		{
			Helper::displayMessage("Computer's Turn...calculating best move.");
			game = AI::BestMove(game);
			game.printBoard();

			game.setPlayersTurn(Human);
			Helper::displayMessage("Player 1 turn...please select a piece to move");
			Continue = true;
		}
	}

	message = "Game Over!\n";
	Helper::displayMessage(message);

	std::system("pause");

	return 1;
}