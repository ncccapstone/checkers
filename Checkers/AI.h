/*
* Module Name: AI.cpp
*
* Description: AI logic for computer player (MiniMax, Negamax)
*
* Author: Team C++
*
* Date: 02NOV2015
*
* Modification History:
*   02NOV2015 - Initial file creation - DA
*	09NOV2015 - Further work on negamax algorithm - DA
*	16NOV2015 - Additional work on return data and AI functions - DA
*	30NOV2015 - Updated functions to use MiniMax algorithm
*	06DEC2015 - AI improvements including setting depth to 6, checking the possible moves for human and switching player's turn - DA
*/

#pragma once

#include <list>
#include "GameBoard.h"

class AI
{
public:
	AI();

	static int Evaluate(GameBoard);
	static std::list<GameBoard> GetDoubleJumps(GameBoard state , COORD jumpingPiece);
	static int MiniMax(const GameBoard &state , int depth , int a , int b , bool max);

	static GameBoard BestMove(GameBoard pState);

	static std::list<GameBoard> possibleMoves(GameBoard&);

	~AI();
private:

};