/*
* Module Name: GameBoard.cpp
*
* Description: C++ class implementation file for GameBoard class
*
* Author: Team C++
*
* Date: 19OCT2015
*
* Modification History:
*   19OCT2015 - Initial file creation
*	25OCT2015 - Added getBoardState function - GR
*	26OCT2015 - Added load/save game functions and Turn enum.  Moved some functions to Helper class - DA
*	02NOV2015 - Added setBoardState function - GR
*	16NOV2015 - Added getter and setter for Turn - DA
*	16NOV2015 - Moved all move and validation functions to here from main application code, doubleJump improvements - GR
*	17NOV2015 - Added revertJump function - GR
*	22NOV2015 - Improved validMove, validJump and doubleJump. - GR
*	06DEC2015 - Removed messaging from validMove function to improve AI - DA
*	13DEC2015 - Code cleanup / add comments / remove redundancy - DA
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <Windows.h>

#include "GameBoard.h"
#include "Helper.h"

#define UpLeft ((newPosX == (oldPosX - 1)) && (newPosY == (oldPosY - 1)))
#define UpRight ((newPosX == (oldPosX + 1)) && (newPosY == (oldPosY - 1)))
#define DownRight ((newPosX == (oldPosX + 1)) && (newPosY == (oldPosY + 1)))
#define DownLeft ((newPosX == (oldPosX - 1)) && (newPosY = (oldPosY + 1)))
#define JumpUL ((newPosX == (oldPosX - 2)) && (newPosY == (oldPosY - 2)))
#define JumpUR ((newPosX == (oldPosX + 2)) && (newPosY == (oldPosY - 2)))
#define JumpDR ((newPosX == (oldPosX + 2)) && (newPosY == (oldPosY + 2)))
#define JumpDL ((newPosX == (oldPosX - 2)) && (newPosY == (oldPosY + 2)))

/*
Red pieces are odd
Black pieces are even
*/

// Default Contsturctor
GameBoard::GameBoard()
{
	gameOver = false;
	turn = Human;

	// Set board to initial state
	for ( int y = 0; y < 8; y++ )
		for ( int x = 0; x < 8; x++ )
		{
			if ( (y == 0 || y == 2) && !(x % 2 == 0) )
				boardState[x][y] = 1;
			else if ( y == 1 && x % 2 == 0 )
				boardState[x][y] = 1;

			else if ( (y == 7 || y == 5) && (x % 2 == 0) )
				boardState[x][y] = 2;
			else if ( y == 6 && !(x % 2 == 0) )
				boardState[x][y] = 2;

			else
				boardState[x][y] = 0;
		}
}

// Display pieces on the board
void GameBoard::printBoard()
{
	COORD pos;
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	HANDLE  hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	for ( int y = 0; y < 8; y++ )
	{
		for ( int x = 0; x < 8; x++ )
		{
			COORD arrayLocation;
			arrayLocation.X = x;
			arrayLocation.Y = y;

			pos.X = Helper::arrayToScreen(arrayLocation).X;
			pos.Y = Helper::arrayToScreen(arrayLocation).Y;

			SetConsoleCursorPosition(hOut , pos);

			switch ( boardState[x][y] ) {
			case Empty:
				std::cout << " ";
				break;
			case Red:
				SetConsoleTextAttribute(hConsole , RED);
				std::cout << "O";
				break;
			case RedKing:
				SetConsoleTextAttribute(hConsole , RED);
				std::cout << KING;
				break;
			case Black:
				SetConsoleTextAttribute(hConsole , WHITE);
				std::cout << "O";
				break;
			case BlackKing:
				SetConsoleTextAttribute(hConsole , WHITE);
				std::cout << KING;
				break;
			}
		}
		std::cout << std::endl;
	}
}

// Load GameBoard from file
void GameBoard::loadGame(std::string fileName)
{
	std::string line;
	std::ifstream loadFile(fileName);

	if ( loadFile.is_open() )
	{
		// Read the board state from file
		for ( auto lineNumber = 0; lineNumber < 8; lineNumber++ )
		{
			if ( getline(loadFile , line) )
			{
				std::stringstream ss(line);

				int pos = 0;

				while ( ss.good() )
				{
					std::string substr;
					getline(ss , substr , ',');

					int value = atoi(substr.c_str());

					boardState[pos][lineNumber] = value;
					pos++;
				}
			}
		}

		// Read the players Turn
		if ( getline(loadFile , line) )
		{
			auto playersTurn = atoi(line.c_str());
			(playersTurn == 0) ? turn = Human : turn = Computer;
		}

		loadFile.close();
		Helper::displayMessage("Game loaded.");
	}
	else Helper::displayMessage("Unable to open file!");
}

// Save GameBoard to file
void GameBoard::saveGame(std::string fileName)
{
	std::ofstream saveFile(fileName);
	if ( saveFile.is_open() )
	{
		for ( auto y = 0; y < 8; y++ )
		{
			for ( auto x = 0; x < 8; x++ )
			{
				saveFile << boardState[x][y];
				if ( x < 7 ) saveFile << ", ";
			}
			saveFile << std::endl;
		}
		saveFile << getPlayersTurn();

		saveFile.close();
	}

	else Helper::displayMessage("Unable to save game to specified file!");
}

// Determine if the game is over
bool GameBoard::isGameOver()
{
	// TODO: Check if a player has no possible moves
	int numRedPieces = 0;
	int numBlackPieces = 0;

	for ( int y = 0; y < 8; y++ )
		for ( int x = 0; x < 8; x++ )
		{
			if ( boardState[x][y] == Red || boardState[x][y] == RedKing )
				numRedPieces++;
			if ( boardState[x][y] == Black || boardState[x][y] == BlackKing )
				numBlackPieces++;
		}

	return (numRedPieces == 0 || numBlackPieces == 0) ? true : false;
}

// Gets the current Turn (Human / Computer)
Turn GameBoard::getPlayersTurn()
{
	return turn;
}

// Sets the current Turn (Human / Computer)
void GameBoard::setPlayersTurn(Turn playersTurn)
{
	turn = playersTurn;
}

// Gets the piece at a given board location
int GameBoard::getBoardState(int x , int y)
{
	return boardState[x][y];
}

// Sets the piece at a given board location
void GameBoard::setBoardState(int x , int y , int newValue)
{
	boardState[x][y] = newValue;
}

//check if new space is empty at a given board location
bool GameBoard::emptySpace(int newPosX , int newPosY)
{
	bool empty = false;

	if ( (getBoardState(newPosX , newPosY) == Empty)
		&& ((0 <= newPosX) && (newPosX < 8) && (0 <= newPosY) && (newPosY < 8)) )
	{
		empty = true;
	}

	return empty;
}

// Checks if a move is valid given the initial and final board positions
bool GameBoard::validMove(COORD selectedPos , COORD newPos)
{
	int oldPosX , oldPosY , newPosX , newPosY;

	oldPosX = selectedPos.X;
	oldPosY = selectedPos.Y;
	newPosX = newPos.X;
	newPosY = newPos.Y;

	if ( !emptySpace(newPosX , newPosY) )
	{
		return false;
	}

	switch ( getBoardState(oldPosX , oldPosY) )
	{
	case Black:
		if ( (((oldPosX + 1) == newPosX) || ((oldPosX - 1) == newPosX))
			&& ((oldPosY - 1) == newPosY) )
		{
			return true;
		}
		else if ( validJump(oldPosX , oldPosY , newPosX , newPosY) )
		{
			return true;
		}
		else
		{
			return false;
		}
		break;
	case BlackKing:
	case RedKing:
		if ( (((oldPosX + 1) == newPosX) || ((oldPosX - 1) == newPosX))
			&& (((oldPosY - 1) == newPosY) || ((oldPosY + 1) == newPosY)) )
		{
			return true;
		}
		else if ( validJump(oldPosX , oldPosY , newPosX , newPosY) )
		{
			return true;
		}
		else
		{
			return false;
		}
		break;
	case Red:
		if ( (((oldPosX + 1) == newPosX) || ((oldPosX - 1) == newPosX))
			&& ((oldPosY + 1) == newPosY) )
		{
			return true;
		}
		else if ( validJump(oldPosX , oldPosY , newPosX , newPosY) )
		{
			return true;
		}
		else
		{
			return false;
		}
		break;
	default:
		return false;
	}

}

// Checks whether a move is a valid jump given the original and final board position
bool GameBoard::validJump(int oldPosX , int oldPosY , int newPosX , int newPosY)
{
	switch ( getBoardState(oldPosX , oldPosY) )
	{
	case Red:
		if ( (((oldPosX + 2) == newPosX) || ((oldPosX - 2) == newPosX))
			&& ((oldPosY + 2) == newPosY) )
		{
			//make sure it's jumping over a black piece
			if ( (JumpDR && (getBoardState(oldPosX + 1 , oldPosY + 1) % 2 == 0) && (!emptySpace(oldPosX + 1 , oldPosY + 1)))
				|| (JumpDL && (getBoardState(oldPosX - 1 , oldPosY + 1) % 2 == 0) && (!emptySpace(oldPosX - 1 , oldPosY + 1))) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
		break;
	case RedKing:
	case BlackKing:
		if ( (((oldPosX + 2) == newPosX) || ((oldPosX - 2) == newPosX))
			&& (((oldPosY + 2) == newPosY) || ((oldPosY - 2) == newPosY)) )
		{
			//make sure the piece being jumped is of the opposite color
			if ( JumpDR && (!emptySpace(oldPosX + 1 , oldPosY + 1)) &&
				(getBoardState(oldPosX + 1 , oldPosY + 1) % 2 != getBoardState(oldPosX , oldPosY) % 2) )
			{
				return true;
			}
			else if ( JumpDL && (!emptySpace(oldPosX - 1 , oldPosY + 1))
				&& (getBoardState(oldPosX - 1 , oldPosY + 1) % 2 != getBoardState(oldPosX , oldPosY) % 2) )
			{
				return true;
			}
			else if ( JumpUR && (!emptySpace(oldPosX + 1 , oldPosY - 1))
				&& (getBoardState(oldPosX + 1 , oldPosY - 1) % 2 != getBoardState(oldPosX , oldPosY) % 2) )
			{
				return true;
			}
			else if ( JumpUL && (!emptySpace(oldPosX - 1 , oldPosY - 1))
				&& (getBoardState(oldPosX - 1 , oldPosY - 1) % 2 != getBoardState(oldPosX , oldPosY) % 2) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
		break;
	case Black:
		if ( (((oldPosX + 2) == newPosX) || ((oldPosX - 2) == newPosX))
			&& ((oldPosY - 2) == newPosY) )
		{
			//make sure the piece being jumped is red
			if ( (JumpUR && (!emptySpace(oldPosX + 1 , oldPosY - 1)) && (getBoardState(oldPosX + 1 , oldPosY - 1) % 2 == 1))
				|| (JumpUL && (!emptySpace(oldPosX - 1 , oldPosY - 1)) && ((getBoardState(oldPosX - 1 , oldPosY - 1) % 2 == 1))) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
		break;
	default:
		return false;
	}
}

//remove the piece that was just jumped
void GameBoard::removePiece(int oldPosX , int oldPosY , int newPosX , int newPosY)
{
	if ( JumpDL )
	{
		setBoardState(oldPosX - 1 , oldPosY + 1 , Empty);
	}
	else if ( JumpDR )
	{
		setBoardState(oldPosX + 1 , oldPosY + 1 , Empty);
	}
	else if ( JumpUL )
	{
		setBoardState(oldPosX - 1 , oldPosY - 1 , Empty);
	}
	else
	{
		setBoardState(oldPosX + 1 , oldPosY - 1 , Empty);
	}
}

// Moves a piece from one position to another (accounting for jumps)
void GameBoard::movePiece(COORD selectedPos , COORD newPos)
{
	int oldPosX , oldPosY , newPosX , newPosY;

	oldPosX = selectedPos.X;
	oldPosY = selectedPos.Y;
	newPosX = newPos.X;
	newPosY = newPos.Y;

	switch ( getBoardState(oldPosX , oldPosY) )
	{
	case Red:
		if ( newPosY == 7 )
		{
			setBoardState(newPosX , newPosY , RedKing);
		}
		else
		{
			setBoardState(newPosX , newPosY , Red);
		}

		if ( jump(selectedPos , newPos) )
		{
			removePiece(oldPosX , oldPosY , newPosX , newPosY);
		}

		break;
	case RedKing:
		setBoardState(newPosX , newPosY , RedKing);
		if ( jump(selectedPos , newPos) )
		{
			removePiece(oldPosX , oldPosY , newPosX , newPosY);
		}
		break;
	case Black:
		if ( newPosY == 0 )
		{
			setBoardState(newPosX , newPosY , BlackKing);
		}
		else
		{
			setBoardState(newPosX , newPosY , Black);
		}
		if ( jump(selectedPos , newPos) )
		{
			removePiece(oldPosX , oldPosY , newPosX , newPosY);
		}

		break;
	case BlackKing:
		setBoardState(newPosX , newPosY , BlackKing);
		if ( jump(selectedPos , newPos) )
		{
			removePiece(oldPosX , oldPosY , newPosX , newPosY);
		}
		break;
	}

	setBoardState(oldPosX , oldPosY , 0);
}

//check if there's a jump available after the first one
bool GameBoard::doubleJump(COORD selectedPos , COORD newPos)
{
	int oldPosX , oldPosY , newPosX , newPosY;
	bool doubleJump;

	oldPosX = selectedPos.X;
	oldPosY = selectedPos.Y;
	newPosX = newPos.X;
	newPosY = newPos.Y;

	movePiece(selectedPos , newPos);

	if ( (emptySpace((newPosX + 2) , (newPosY + 2))) &&
		(validJump(newPosX , newPosY , newPosX + 2 , newPosY + 2)) )
	{
		doubleJump = true;
	}
	else if ( (emptySpace((newPosX + 2) , (newPosY - 2))) &&
		(validJump(newPosX , newPosY , newPosX + 2 , newPosY - 2)) )
	{
		doubleJump = true;
	}
	else if ( (emptySpace((newPosX - 2) , (newPosY + 2))) &&
		(validJump(newPosX , newPosY , newPosX - 2 , newPosY + 2)) )
	{
		doubleJump = true;
	}
	else if ( (emptySpace((newPosX - 2) , (newPosY - 2))) &&
		(validJump(newPosX , newPosY , newPosX - 2 , newPosY - 2)) )
	{
		doubleJump = true;
	}
	else
	{
		doubleJump = false;
	}

	revertJump(newPos , selectedPos);
	return doubleJump;
}


// Check if a movement is a jump given the initial and final board positions
bool GameBoard::jump(COORD selectedPos , COORD newPos)
{
	int oldPosX = selectedPos.X;
	int oldPosY = selectedPos.Y;
	int newPosX = newPos.X;
	int newPosY = newPos.Y;

	if ( (JumpDL) || (JumpDR) || (JumpUL) || (JumpUR) )
	{
		return true;
	}

	return false;
}

//revert the pieces if the user decides not to continue with double jump - may not be needed
void GameBoard::revertJump(COORD movedPos , COORD originalPos)
{
	int oldPosX , oldPosY , newPosX , newPosY;

	oldPosX = movedPos.X;
	oldPosY = movedPos.Y;
	newPosX = originalPos.X;
	newPosY = originalPos.Y;

	switch ( getBoardState(oldPosX , oldPosY) )
	{
	case Red:
		setBoardState(newPosX , newPosY , Red);

		if ( JumpUL )
		{
			setBoardState(oldPosX - 1 , oldPosY - 1 , Black);
		}
		else if ( JumpUR )
		{
			setBoardState(oldPosX + 1 , oldPosY - 1 , Black);
		}
		break;

	case RedKing:
		setBoardState(newPosX , newPosY , RedKing);

		if ( JumpDL )
		{
			setBoardState(oldPosX - 1 , oldPosY + 1 , Black);
		}
		else if ( JumpDR )
		{
			setBoardState(oldPosX + 1 , oldPosY + 1 , Black);
		}
		else if ( JumpUL )
		{
			setBoardState(oldPosX - 1 , oldPosY - 1 , Black);
		}
		else
		{
			setBoardState(oldPosX + 1 , oldPosY - 1 , Black);
		}
		break;

	case Black:
		setBoardState(newPosX , newPosY , Black);
		if ( JumpDL )
		{
			setBoardState(oldPosX - 1 , oldPosY + 1 , Red);
		}
		else if ( JumpDR )
		{
			setBoardState(oldPosX + 1 , oldPosY + 1 , Red);
		}
		break;

	case BlackKing:
		setBoardState(newPosX , newPosY , BlackKing);

		if ( JumpDL )
		{
			setBoardState(oldPosX - 1 , oldPosY + 1 , Red);
		}
		else if ( JumpDR )
		{
			setBoardState(oldPosX + 1 , oldPosY + 1 , Red);
		}
		else if ( JumpUL )
		{
			setBoardState(oldPosX - 1 , oldPosY - 1 , Red);
		}
		else
		{
			setBoardState(oldPosX + 1 , oldPosY - 1 , Red);
		}
		break;
	}

	setBoardState(oldPosX , oldPosY , 0);
}

// Default Destructor
GameBoard::~GameBoard()
{

}