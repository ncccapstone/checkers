#include <windows.h>;

class Piece
{
private:

	//	int x;
	//	int y;
	COORD location;
	Color color;
	bool isKing();

public:
	Piece(COORD, Color);
	~Piece();
	int getValue();
	COORD getLocation();
	void setLocation(int, int);
	void movePiece(Piece, COORD);
	Color getColor();
};

enum Color { Red, Black };
