/*
* Module Name: Helper.cpp
*
* Description: Implementation file for Helper class
*
* Author: Team C++
*
* Date: 26OCT2015
*
* Modification History:
*   26OCT2015 - Initial file creation - DA
*	16NOV2015 - Added displayMessage function - DA
*	23NOV2015 - Improved message display - GR
*/

#include <Windows.h>

#pragma once

#define TL static_cast<char>(218)
#define TR static_cast<char>(191)
#define BL static_cast<char>(192)
#define BR static_cast<char>(217)

#define VL static_cast<char>(179)
#define HL static_cast<char>(196)

#define TB static_cast<char>(194)
#define BB static_cast<char>(193)
#define LB static_cast<char>(195)
#define RB static_cast<char>(180)

#define IB static_cast<char>(197)

#define ESC 27

#define LEFT_ARROW 37
#define UP_ARROW 38
#define RIGHT_ARROW 39
#define DOWN_ARROW 40

#define KING static_cast<char>(232) //  OR 233?

#define RED 12
#define WHITE 15

enum Turn { Human , Computer };

enum PieceType { Empty , Red , Black , RedKing , BlackKing };

class Helper {
public:
	static void PrintBoard();

	static BOOL SetConsoleFontSize(COORD);
	static void ShowConsoleCursor(bool);

	static void clearTextInput();
	static void displayMessage(std::string);

	static COORD screenToArray(COORD);
	static COORD arrayToScreen(COORD);
};
